export default {
  actionIcon: 'StarIcon',
  highlightColor: 'warning',
  data: [
    {index: 0, label: 'Home', url: '/', labelIcon: 'HomeIcon', highlightAction: false},
    {index: 1, label: 'Email', url: '/email', labelIcon: 'MailIcon', highlightAction: false},
    {index: 2, label: 'Chat Mateus', url: '/chat', labelIcon: 'MessageSquareIcon', highlightAction: false},
    {index: 3, label: 'Lista de Tarefas', url: '/tarefas', labelIcon: 'CheckSquareIcon', highlightAction: false},
    {index: 4, label: 'Calendario', url: '/calendario', labelIcon: 'CalendarIcon', highlightAction: false},
  ]

}
