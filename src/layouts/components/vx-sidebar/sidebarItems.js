/*=========================================================================================
  File Name: sidebarItems.js
  Description: Sidebar Items list. Add / Remove menu items from here.
  Strucutre:
          url     => router path
          name    => name to display in sidebar
          slug    => router path name
          icon    => Feather Icon component/icon name
          tag     => text to display on badge
          tagColor  => class to apply on badge element
          i18n    => Internationalization
          submenu   => submenu of current item (current item will become dropdown )
                NOTE: Submenu don't have any icon(you can add icon if u want to display)
          isDisabled  => disable sidebar item/group
  ----------------------------------------------------------------------------------------
  Item Name: Vuesax Admin - VueJS Dashboard Admin Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/


export default [{
    url: "/",
    name: "Dashboard",
    slug: "home",
    icon: "HomeIcon",
    /* MODELO DE SUBMENU 
    submenu: [
			{
				url: '/dashboard/analytics',
				name: "Análise de Dados",
				slug: "dashboardAnalytics",
				i18n: "Analytics",
			},
			{
				url: '/dashboard/ecommerce',
				name: "eCommerce",
				slug: "dashboardECommerce",
				i18n: "eCommerce",
			},
		] */
  },
  {
    header: "Aplicações",
    i18n: "Apps",
  },
  {
    url: "/email",
    name: "Email",
    slug: "email",
    icon: "MailIcon",
  },
  {
    url: "/chat",
    name: "Chat Mateus",
    slug: "chat",
    icon: "MessageSquareIcon",
  },
  {
    url: "/tarefas",
    name: "Lista de Tarefas",
    slug: "tarefas",
    icon: "CheckSquareIcon",
  },
  {
    url: "/calendario",
    name: "Calendário",
    slug: "calendario",
    icon: "CalendarIcon",
  },
  {
    url: "/consolidacaobaixas",
    name: "Baixas",
    slug: "baixas",
    icon: "CalendarIcon",
  }
]
